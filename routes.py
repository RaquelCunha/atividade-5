from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Raquel'}
    posts = [
        {
            'author': {'username': 'Fernando'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'username': 'Talita'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return render_template('index.html', title='Home', user=user, posts=posts)